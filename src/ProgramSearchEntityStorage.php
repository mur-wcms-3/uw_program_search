<?php

namespace Drupal\uw_program_search;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\uw_program_search\Entity\ProgramSearchEntityInterface;

/**
 * Defines the storage handler class for Program entities.
 *
 * This extends the base storage class, adding required special handling for
 * Program entities.
 *
 * @ingroup uw_program_search
 */
class ProgramSearchEntityStorage extends SqlContentEntityStorage implements ProgramSearchEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ProgramSearchEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {program_search_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {program_search_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(ProgramSearchEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {program_search_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('program_search_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
