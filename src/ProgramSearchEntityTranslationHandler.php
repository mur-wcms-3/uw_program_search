<?php

namespace Drupal\uw_program_search;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for program_search_entity.
 */
class ProgramSearchEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
