<?php

namespace Drupal\uw_program_search\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\uw_program_search\Entity\ProgramSearchEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProgramSearchEntityController.
 *
 *  Returns responses for Program routes.
 */
class ProgramSearchEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Program revision.
   *
   * @param int $program_search_entity_revision
   *   The Program revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($program_search_entity_revision) {
    $program_search_entity = $this->entityTypeManager()->getStorage('program_search_entity')
      ->loadRevision($program_search_entity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('program_search_entity');

    return $view_builder->view($program_search_entity);
  }

  /**
   * Page title callback for a Program revision.
   *
   * @param int $program_search_entity_revision
   *   The Program revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($program_search_entity_revision) {
    $program_search_entity = $this->entityTypeManager()->getStorage('program_search_entity')
      ->loadRevision($program_search_entity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $program_search_entity->label(),
      '%date' => $this->dateFormatter->format($program_search_entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Program.
   *
   * @param \Drupal\uw_program_search\Entity\ProgramSearchEntityInterface $program_search_entity
   *   A Program object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ProgramSearchEntityInterface $program_search_entity) {
    $account = $this->currentUser();
    $program_search_entity_storage = $this->entityTypeManager()->getStorage('program_search_entity');

    $langcode = $program_search_entity->language()->getId();
    $langname = $program_search_entity->language()->getName();
    $languages = $program_search_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $program_search_entity->label()]) : $this->t('Revisions for %title', ['%title' => $program_search_entity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all program revisions") || $account->hasPermission('administer program entities')));
    $delete_permission = (($account->hasPermission("delete all program revisions") || $account->hasPermission('administer program entities')));

    $rows = [];

    $vids = $program_search_entity_storage->revisionIds($program_search_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\uw_program_search\ProgramSearchEntityInterface $revision */
      $revision = $program_search_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $program_search_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.program_search_entity.revision', [
            'program_search_entity' => $program_search_entity->id(),
            'program_search_entity_revision' => $vid,
          ]));
        }
        else {
          $link = $program_search_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.program_search_entity.translation_revert', [
                'program_search_entity' => $program_search_entity->id(),
                'program_search_entity_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.program_search_entity.revision_revert', [
                'program_search_entity' => $program_search_entity->id(),
                'program_search_entity_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.program_search_entity.revision_delete', [
                'program_search_entity' => $program_search_entity->id(),
                'program_search_entity_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['program_search_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
