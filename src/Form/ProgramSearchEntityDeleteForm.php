<?php

namespace Drupal\uw_program_search\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Program entities.
 *
 * @ingroup uw_program_search
 */
class ProgramSearchEntityDeleteForm extends ContentEntityDeleteForm {


}
