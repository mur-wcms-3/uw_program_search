<?php

namespace Drupal\uw_program_search\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Program revision.
 *
 * @ingroup uw_program_search
 */
class ProgramSearchEntityRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Program revision.
   *
   * @var \Drupal\uw_program_search\Entity\ProgramSearchEntityInterface
   */
  protected $revision;

  /**
   * The Program storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $programSearchEntityStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->programSearchEntityStorage = $container->get('entity_type.manager')->getStorage('program_search_entity');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'program_search_entity_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.program_search_entity.version_history', ['program_search_entity' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $program_search_entity_revision = NULL) {
    $this->revision = $this->ProgramSearchEntityStorage->loadRevision($program_search_entity_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->ProgramSearchEntityStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Program: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Program %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.program_search_entity.canonical',
       ['program_search_entity' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {program_search_entity_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.program_search_entity.version_history',
         ['program_search_entity' => $this->revision->id()]
      );
    }
  }

}
