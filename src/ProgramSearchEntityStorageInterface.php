<?php

namespace Drupal\uw_program_search;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\uw_program_search\Entity\ProgramSearchEntityInterface;

/**
 * Defines the storage handler class for Program entities.
 *
 * This extends the base storage class, adding required special handling for
 * Program entities.
 *
 * @ingroup uw_program_search
 */
interface ProgramSearchEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Program revision IDs for a specific Program.
   *
   * @param \Drupal\uw_program_search\Entity\ProgramSearchEntityInterface $entity
   *   The Program entity.
   *
   * @return int[]
   *   Program revision IDs (in ascending order).
   */
  public function revisionIds(ProgramSearchEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Program author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Program revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\uw_program_search\Entity\ProgramSearchEntityInterface $entity
   *   The Program entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ProgramSearchEntityInterface $entity);

  /**
   * Unsets the language for all Program with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
