<?php

namespace Drupal\uw_program_search;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Program entities.
 *
 * @ingroup uw_program_search
 */
class ProgramSearchEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Program ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\uw_program_search\Entity\ProgramSearchEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.program_search_entity.edit_form',
      ['program_search_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
