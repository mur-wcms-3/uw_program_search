<?php

/**
 * @file
 * Contains program_search_entity.page.inc.
 *
 * Page callback for Program entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Program templates.
 *
 * Default template: program_search_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_program_search_entity(array &$variables) {
  // Fetch ProgramSearchEntity Entity Object.
  $program_search_entity = $variables['elements']['#program_search_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
